# Default parameters for the openstackclient

export OS_AUTH_URL=${OS_AUTH_URL:-https://keystone.cern.ch/v3}
export OS_IDENTITY_API_VERSION=${OS_IDENTITY_API_VERSION:-3}
export OS_VOLUME_API_VERSION=${OS_VOLUME_API_VERSION:-2}
export OS_PROJECT_DOMAIN_ID=${OS_PROJECT_DOMAIN_ID:-default}
export OS_PROJECT_NAME=${OS_PROJECT_NAME:-"Personal $JUPYTERHUB_USER"}
export OS_AUTH_TYPE=${OS_AUTH_TYPE:-'v3fedkerb'}
export OS_MUTUAL_AUTH=${OS_MUTUAL_AUTH:-'disabled'}
export OS_IDENTITY_PROVIDER=${OS_IDENTITY_PROVIDER:-'sssd'}
export OS_PROTOCOL=${OS_PROTOCOL:-'kerberos'}
export OS_REGION_NAME=${OS_REGION_NAME:-'cern'}

