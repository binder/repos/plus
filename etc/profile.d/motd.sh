#!/bin/sh

cat << EOF
      _____ __    _____ _____      _   
     |  _  |  |  |  |  |   __|   _| |_ 
     |   __|  |__|  |  |__   |  |_   _|
     |__|  |_____|_____|_____|    |_|  

Welcome to your self-contained CERN Plus environment.
Get started with kinit ${JUPYTERHUB_USER} and proceed with your
favorite tools.

Session Limits - CPU: ${CPU_LIMIT} Memory: $(awk "BEGIN{printf \"%.1f\", ${MEM_LIMIT} / 1024.0 / 1024.0 / 1024.0}") GB

For more info on usage and how to customize this image, visit:

        https://gitlab.cern.ch/binder/repos/plus

EOF
